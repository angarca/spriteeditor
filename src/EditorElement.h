#ifndef SPRITEEDITOR_ELEMENT_H
#define SPRITEEDITOR_ELEMENT_H

#include <UI/Element.h>

class EditorElement : public randomize::UI::Element{
protected:
	GLfloat color[4];
public:
	virtual void Draw() override;
	void setColor( GLfloat c[4]);
	GLfloat nextFrameChange( GLfloat t);
	void setNewRect( GLint despX, GLint despY, GLuint tamW, GLuint tamH, GLfloat t);
	void loadFromIFL( const char *path);
	void saveToSFL( const char *path);
};

#endif//SPRITEEDITOR_ELEMENT_H

