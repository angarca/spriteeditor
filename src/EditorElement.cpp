
#include "EditorElement.h"

void EditorElement::Draw()
{
	GLfloat vertices[] = {
		-0.5,-0.5, -1,
		-0.5, 0.5, -1,	
		 0.5, 0.5, -1,
		 0.5,-0.5, -1
	}, tmpX, tmpY, tmpW, tmpH;	
	
	glPushMatrix();
	glTranslatef(pos_x,pos_y,0);
	glRotatef(degree, 0,0,1);
	glScalef(tam_w,tam_h,1);

	for(int i = 0; i < v_capa.size(); i++)
	{
		if(v_capa[i].isActivo())
		{
			v_capa[i].getPos(tmpX,tmpY);
			v_capa[i].getSize(tmpW,tmpH);
			glPushMatrix();
			glTranslatef(tmpX,tmpY,0);
			glScalef(tmpW,tmpH,1);

			if( v_capa[i].isColisionable()){
				GLfloat tmp[4];
				glDisable(GL_TEXTURE_2D);
				glGetFloatv( GL_CURRENT_COLOR, tmp);
				glColor4fv( this->color);
				glBegin( GL_LINE_LOOP);
				glVertex3f( -0.5,-0.5,-1);
				glVertex3f( -0.5, 0.5,-1);
				glVertex3f(  0.5, 0.5,-1);
				glVertex3f(  0.5,-0.5,-1);
				glEnd();
				glColor4fv( tmp);
				glEnable(GL_TEXTURE_2D);
			}

			if( v_capa[i].getVisible()){
				glActiveTexture( GL_TEXTURE0);
				v_capa[i].BindTexture();
			
				glEnableClientState(GL_VERTEX_ARRAY);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			
				glVertexPointer(3, GL_FLOAT, 0, vertices);
				v_capa[i].TexCoordPointer();

				glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 );
			
				glDisableClientState(GL_VERTEX_ARRAY);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			}

			glPopMatrix();
		}
		
	}
	
	glPopMatrix();

}

void EditorElement::setColor( GLfloat c[4]){
	for( GLuint i = 0; i < 4; i++)
		this->color[i] = c[i];
}

GLfloat EditorElement::nextFrameChange( GLfloat t){
	GLuint i, j, size = this->v_capa.size(), subSize;
	GLfloat tmp, min, precision = 0.000001;
//std::cout << "n1: " << t << " " << size << std::endl;
	for( i=0; i<size; i++){
		subSize = this->v_capa[i].getSize();
		tmp = 0.0;
//std::cout << "n2: " << subSize << std::endl;
		for( j=0; j<subSize && tmp <= t; j++)
			tmp += this->v_capa[i].getFrameDuration( j);
//std::cout << "n3: " << i << " " << j << " " << tmp << std::endl;
		if( i == 0 || (tmp > t + precision && tmp < min)) min = tmp;
	}
//std::cout << "n4: " << min << std::endl;
	return min;
}

void EditorElement::setNewRect( GLint despX, GLint despY, GLuint tamW, GLuint tamH, GLfloat t){
//std::cout << "uno: " << despX << " " << despY << " " << tamW << " " << tamH << " " << t << std::endl;
	GLuint i, j, size = this->v_capa.size(), w, h, p = 0, w2, h2, tmp;
	GLfloat d[3] = { this->v_capa[0].getTotalDuration(), this->nextFrameChange( t) - t, 0};
	if( t < d[0]){
//std::cout << "dos: " << size << " " << p << " " << d[0] << " " << d[1] << " " << d[2] << std::endl;
		this->v_capa[0].reset().getSize( w, h);
		if( t == 0.0){
//std::cout << "tres: " << w << " " << h << std::endl;
			this->v_capa.resize( size + 1);
			this->v_capa[size].addFrame( 0, 0, 0, w, h, 0.0, false);
			this->v_capa[size].addFrame( 0, 0, 0, 0, 0, d[0], false);
			this->v_capa[size].setActivo( true);
			i = size;
		}else{
//std::cout << "cuatro: " << w << " " << h << std::endl;
			for( i=1; i<size; i++){
				tmp = this->v_capa[i].reset().onStep(t).getSize( w2, h2).getCurrent();
				d[2] = 0;
				for( j=0; j <= tmp; j++)
					d[2] += this->v_capa[i].getFrameDuration( j);
				if( w2 == 0 && h2 == 0 && d[2] >= d[1] + t)
					p = i;
			}
			if( p == 0){
//std::cout << "cinco: " << i << " " << d[1] << " " << d[2] << std::endl;
				this->v_capa.resize( size + 1);
				this->v_capa[size].addFrame( 0, 0, 0, w, h, 0.0, false);
				this->v_capa[size].addFrame( 0, 0, 0, 0, 0, d[0], false);
				this->v_capa[size].setActivo( true);
				i = size;
			}else
				i = p;
		}
//std::cout << "siete: " << i << " " << this->nextFrameChange(t) << std::endl;
		this->v_capa[i].insertFrame( 0, despX, despY, tamW, tamH, t, d[1], false);
	}
//std::cout << "ocho.\n";
}

void EditorElement::loadFromIFL( const char *path){
	this->addCapa( 0, path);
	this->v_capa[0].setColisionable( false);
}

void EditorElement::saveToSFL( const char *path){
	GLuint i, size = this->v_capa.size();
	std::string tmp;
	std::ofstream file( path);
	file << this->v_capa[0].getFileName() << " NO_COLISIONABLE\n";
	for( i=1; i<size; i++){
		tmp = this->v_capa[i].getFileName();
		if( tmp == ""){
			tmp = std::string(path) + ".BB" + std::to_string(i) + ".ifl";
			this->v_capa[i].setFileName( tmp.c_str());
		}
//		file << tmp << " COLISIONABLE\n";
//		this->v_capa[i].saveToIFL();
		file << "INLINE\n";
		this->v_capa[i].saveToIFL( file);
		file << "\tCOLISIONABLE\n";
	}
	file.close();
}






