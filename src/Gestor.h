
#ifndef SPRITEEDITOR_GESTOR_H
#define SPRITEEDITOR_GESTOR_H


#include <stdexcept>
#include <log/log.h>
#include <vgm/Ventana.h>
#include "UserInterface.h"

class Gestor: public Ventana{
public:
	Gestor(int width = 640, int height = 480);
	void initGL() override;
	void onStep(float) override;
	void onDisplay() override;
	void onKeyPressed(SDL_Event &e) override;
	void onPressed(const Pulsado &p) override;
	void onMouseMotion(SDL_Event &e) override;
	void onMouseButton(SDL_Event &e) override;

	std::string help();
	void setInterface( UserInterface *interface){ this->interface = interface;}
private:
	UserInterface *interface;
	GLfloat timeStep;
	GLuint w, h;
};

#endif //SPRITEEDITOR_GESTOR_H
