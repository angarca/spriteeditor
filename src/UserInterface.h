#ifndef SPRITEEDITOR_USERINTERFACE_H
#define SPRITEEDITOR_USERINTERFACE_H

#include <iostream>
#include "EditorElement.h"

class UserInterface{
private:
	EditorElement element;
	GLfloat t;
	GLuint w, h;
	GLint posXini, posYini, posXcurrent, posYcurrent;
	GLboolean leftClickPressed;
public:
	UserInterface():t(0.0),leftClickPressed(false){}
	UserInterface &addTime( GLfloat dt){ t += dt; if(t<0.0)t=0.0; return *this;}
	UserInterface &loadFromIFL( const char *path);
	UserInterface &loadFromSFL( const char *path);
	UserInterface &save( const char *path);
	UserInterface &draw();
	GLboolean rightClickInside( GLint x, GLint y);
	UserInterface &setCollisionColor( GLfloat c[4]){ this->element.setColor( c); return *this;}
	UserInterface &nextFrameChange(){ this->t = this->element.nextFrameChange( this->t); return *this;}
	UserInterface &leftClick( GLint x, GLint y, GLint w, GLint h, GLboolean buttonDown);
	UserInterface &mouseMotion(  GLint x, GLint y, GLint w, GLint h);
};

#endif//SPRITEEDITOR_USERINTERFACE_H
