#include <string>
#include <utils/SignalHandler.h>
#include "Gestor.h"
#include "UserInterface.h"

void usage();
char extension( const std::string &filename);
std::string autoDetectExtension( const char *filename, std::string &entrada, bool &tipoIFL);

int main(int argc, char *argv[]){
	UserInterface interface;
	std::string ifl("-i"), sfl("-s"), entrada, salida;
	bool tipoIFL;

	// this prints in cout the stack trace if there is a segfault. try dereferencing a null in an inner function
	SigsegvPrinter::activate(std::cout);

	if( argc < 2) usage();
	else if( argc == 2) salida = autoDetectExtension( argv[1], entrada, tipoIFL);
	else if( argc == 3)
		if( ifl == argv[1]){ tipoIFL = true; entrada = argv[2]; salida = std::string( argv[2]) + ".sfl";}
		else if( sfl == argv[1]){ tipoIFL = false; entrada = argv[2]; salida = argv[2];}
		else{ autoDetectExtension( argv[1], entrada, tipoIFL); salida = argv[2];}
	else if( argc == 4)
		if( ifl == argv[1]){ tipoIFL = true; entrada = argv[2]; salida = argv[3];}
		else if( sfl == argv[1]){ tipoIFL = false; entrada = argv[2]; salida = argv[3];}
		else usage();
	else usage();


	if( salida != ""){
		GLfloat white[4] = { 1, 1, 1, 1};
		Gestor gestor(1300, 700);
		if( tipoIFL) interface.loadFromIFL( entrada.c_str());
		else interface.loadFromSFL( entrada.c_str());
		interface.setCollisionColor( white);
		gestor.setInterface( &interface);
		gestor.mainLoop();
		interface.save( salida.c_str());
	}

	std::cout << endl;
	return 0;
}

void usage(){
	std::cout << "Usage: [-i|-s] <input file.(ifl|sfl)> [<output file>]\n";
}

char extension( const std::string &filename){
	unsigned int size = filename.size();
	if( size >= 4 && filename[size-1] == 'l' && filename[size-2] == 'f' && filename[size-4] == '.')
		if( filename[size-3] == 'i' || filename[size-3] == 's') return filename[size-3];
	return '\0';
}

std::string autoDetectExtension( const char *filename, std::string &entrada, bool &tipoIFL){
	std::string str;	char c;
	if( (c = extension( filename)) == 'i'){ tipoIFL = true; entrada = filename; str = std::string( filename) + ".sfl";}
	else if( c == 's'){ tipoIFL = false; entrada = filename; str = filename;}
	else usage();
	return str;
}

