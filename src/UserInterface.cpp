#include "UserInterface.h"

UserInterface &UserInterface::loadFromIFL( const char *path){
	this->element.loadFromIFL( path);
	return *this;
}

UserInterface &UserInterface::loadFromSFL( const char *path){
	this->element.loadFromSFL( path);
	return *this;
}

UserInterface &UserInterface::save( const char *path){
	this->element.saveToSFL( path);
	return *this;
}

UserInterface &UserInterface::draw(){
	this->element.reset();
	this->element.onStep( this->t);
	randomize::UI::BeginDraw();
	this->element.Draw();
	if( this->leftClickPressed){
		GLfloat tmp[4], blue[4] = { 0, 0, 1, 1}, esc = (this->w > this->h) ? this->h : this->w;
		glPushMatrix();
		glScalef( 100/esc, 100/esc, 1);
		glDisable(GL_TEXTURE_2D);
		glGetFloatv( GL_CURRENT_COLOR, tmp);
		glColor4fv( blue);
		glBegin( GL_LINE_LOOP);
		glVertex3f( this->posXini, this->posYini,-1);
		glVertex3f( this->posXini, this->posYcurrent,-1);
		glVertex3f( this->posXcurrent, this->posYcurrent,-1);
		glVertex3f( this->posXcurrent, this->posYini,-1);
		glEnd();
		glColor4fv( tmp);
		glEnable(GL_TEXTURE_2D);
		glPopMatrix();
	}
	randomize::UI::EndDraw();
	return *this;
}

GLboolean UserInterface::rightClickInside( GLint x, GLint y){
	return this->element.inside( x, y);
}

UserInterface &UserInterface::leftClick( GLint x, GLint y, GLint w, GLint h, GLboolean buttonDown){
	this->leftClickPressed = buttonDown;
	if( this->leftClickPressed){
		this->posXcurrent = this->posXini =  x - w/2;
		this->posYcurrent = this->posYini = -y + h/2;
		this->w = w; this->h = h;
	}else{
		GLint despX, despY, tamW, tamH;
		this->posXcurrent =  x - w/2;
		this->posYcurrent = -y + h/2;
		if( this->posXini > this->posXcurrent){
			tamW = this->posXini - this->posXcurrent;
			despX = (this->posXini + this->posXcurrent) / 2;
		}else{
			tamW = this->posXcurrent - this->posXini;
			despX = (this->posXcurrent + this->posXini) / 2;
		}
		if( this->posYini > this->posYcurrent){
			tamH = this->posYini - this->posYcurrent;
			despY = (this->posYini + this->posYcurrent) / 2;
		}else{
			tamH = this->posYcurrent - this->posYini;
			despY = (this->posYcurrent + this->posYini) / 2;

		}
		this->element.setNewRect( despX, despY, tamW, tamH, this->t);
	}
	return *this;
}

UserInterface &UserInterface::mouseMotion(  GLint x, GLint y, GLint w, GLint h){
	this->posXcurrent =  x - w/2;
	this->posYcurrent = -y + h/2;
	return *this;	
}


