
#include "Gestor.h"

Gestor::Gestor(int width, int height): Ventana(width, height), timeStep(0.01), w(width), h(height){
	SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
	initGL();

	SDL_SetWindowTitle( this->window, "spriteEditor");
	randomize::UI::setConfWindow(width, height);

	this->setFps(30);

	semaforoStep.sumar();
	semaforoDisplay.sumar();
}

void Gestor::initGL() {
	int width = 640, height = 480;

	if (window == NULL) {
		SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
			"There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
	} else {
		SDL_GetWindowSize( this->window, &width, &height);
		this->context = SDL_GL_CreateContext( this->window);
		if ( !(this->context)) {
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
			SDL_Quit();
			exit(2);
		}
	}

	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);	// This Will Clear The Background Color To Black
	glShadeModel(GL_SMOOTH);		// Enables Smooth Color Shading
	glPointSize(3);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                // Reset The Projection Matrix

	this->perspectiveGL (45, GLdouble(width) / height, 0.1, 100);

	glMatrixMode(GL_MODELVIEW);
}

void Gestor::onStep(float dt) {}

void Gestor::onDisplay() {

	// Clear The Screen And The Depth Buffer
	glClear( GL_COLOR_BUFFER_BIT );

	glLoadIdentity();                // Reset The View
	glPushMatrix();
	this->interface->draw();
	glPopMatrix();

	// swap buffers to display, since we're double buffered.
	SDL_GL_SwapWindow( this->window);
}

std::string Gestor::help() {
	return std::string("Controls:\n"
			"h: print this help.\n"
			"n: pass until next change of a frame.\n"
			"RIGHT_ARROW: pass time, LEFT_ARROW: rewind time.\n"
			"RIGHT_CLICK: test inside.\n");
}

void Gestor::onPressed(const Pulsado &p) {
	if ( p.sym == SDLK_RIGHT ||
	     p.sym == SDLK_LEFT  )
		semaforoDisplay.sumar();

	switch(p.sym) {
	case SDLK_RIGHT:
		interface->addTime( this->timeStep);
		break;
	case SDLK_LEFT:
		interface->addTime( -(this->timeStep));
		break;
	default:;
	}
}
void Gestor::onKeyPressed(SDL_Event &e) {
	semaforoDisplay.sumar();
	if (e.type == SDL_KEYUP) {
		return;
	}

	switch(e.key.keysym.sym) {
	case SDLK_h:
		std::cout << this->help() << endl;
		break;
	case SDLK_n:
		this->interface->nextFrameChange();
		break;
	default:;
	}
}

void Gestor::onMouseButton(SDL_Event &e){
	GLfloat white[4] = { 1, 1, 1, 1}, red[4] = { 1, 0, 0, 1}, green[4] = { 0, 1, 0, 1};

	if( e.button.button == SDL_BUTTON_RIGHT)
		if( e.button.type == SDL_MOUSEBUTTONDOWN)
			if( this->interface->rightClickInside( e.button.x, e.button.y))
				this->interface->setCollisionColor( green);
			else
				this->interface->setCollisionColor( red);
		else
			this->interface->setCollisionColor( white);

	if( e.button.button == SDL_BUTTON_LEFT) {
		this->interface->leftClick(e.button.x, e.button.y, this->w, this->h, e.button.type == SDL_MOUSEBUTTONDOWN);
	}

	semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e){
	this->interface->mouseMotion( e.button.x, e.button.y, this->w, this->h);
}



