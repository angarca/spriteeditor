# README #

This is a simple tool, for editing sprites to be used by https://bitbucket.org/mutcoll/common-libs/, with randomize::UI::Element class.

To build it you need to install first https://www.biicode.com/ then

```
bii init new_dir -l clion
cd new_dir
bii setup:cpp
bii open randomize/common-libs
bii configure -DCMAKE_BUILD_TYPE=RELWITHDEBINFO
cd blocks
mkdir angarca
cd angarca
git clone https://angarca@bitbucket.org/angarca/spriteeditor.git
mv spriteeditor spriteEditor
bii build
cd ../../bin
```

And finally here is the binary named angarca_spriteEditor_src_main

To use it you need at least one custom .ifl file, you can make it with https://bitbucket.org/angarca/ifl-conversor once you got it, this is the command line that accepts ./angarca_spriteEditor_src_main [-i|-s] <input file.(ifl|sfl)> [<output file>]

With the program running the 'h' key display some helpful commands in the terminal. Try it, and have a lot of fun customizing your sprite`s bounding boxes.